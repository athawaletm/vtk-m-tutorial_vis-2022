# VTK-m Tutorial_VIS 2022

## About This Page

This page contains materials for the VTK-m tutorial. This tutorial will be presented on Monday, October 17th at the VIS22 conference in Oklahoma, USA. 


## Slack Channel
Please join the slack channel now if you are planning to attend the tutorial:

[Slack channel link](https://vtk-mtutorialvis2022.slack.com/archives/C0413K9H8H1) 

We will use the slack channel to communicate with participants and answer their questions.

## VTK-m Tutorial Material
Attendees can get the slides [HERE](https://www.dropbox.com/s/4pp4xf1jlvlt4th/VTKm_Tutorial_VIS22.pptx?dl=0) and the user guide is [HERE](https://gitlab.kitware.com/vtk/vtk-m-user-guide/-/wikis/uploads/VTKmUsersGuide-1-9.pdf).


## VTK-m Installations Options

There are three options for running code:

1. Building VTK-m on your own machine.
2. Running our VirtualBox image with VTK-m installed.
3. Running Docker image with VTK-m installed.

Sections below describe how to pursue each of these options.

### Download and Build VTK-m 

To build VTK-m directly on your laptop, you will need at a minimum CMake and, of course, a C++ compiler.
(The [VTK-m dependencies list](https://gitlab.kitware.com/vtk/vtk-m#dependencies) has details.)

The process to build VTK-m:

Download [vtk-m-src-tut-2022.tar.gz](https://www.dropbox.com/s/hw6zjfal6rxroh0/vtk-m-src-tut-2022.tar.gz?dl=1) tarball to get the bundled VTK-m source code and examples we will be using in this tutorial.
(Alternately, you can get the [vtk-m-src-tut-2022.zip](https://www.dropbox.com/s/6amhvwpe4g5dzm0/vtk-m-src-tut-2022.zip?dl=1) zip archive.)[^gitdownload]
Once you have the archive, you can unpack it and compile VTK-m along with the tutorial examples.
Your process on Unix/Mac should be something like:

```sh
# (download vtk-m-src-tut-2022.tar.gz)
tar xf vtk-m-src-tut-2022.tar.gz
mkdir vtk-m-build
cd vtk-m-build
cmake ../vtk-m -DVTKm_ENABLE_TUTORIALS=ON
make -j8
```

The above configuration is the minimal build with the tutorials.
There are several other CMake configuration options including specific device support.
However, we recommend not enabling the TBB, Kokkos, or CUDA support, at least at first.
(We stand behind our support for these backends, but it is good to start simple).

### Running our VirtualBox Image with VTK-m Installed.

The Virtual Box image with VTK-m installed can be found [here](https://www.dropbox.com/s/c84fk04zdfumwxa/VTK-m%20Tutorial%20%40%20VIS%202022.ova?dl=0).

To be able to use this image you'll need to have the VirtualBox software (**recommended version 6.1**) installed. Please follow the instructions provided [here](https://www.virtualbox.org/wiki/Download_Old_Builds_6_1).

The internet connection at the tutorial venue may discourage the requested downloads, in that case please ask the presenters for a flash drive with the required material.

The username to access the material on the VM is vtkm-user.
The VM should automatically log into this account.

VTK-m source code and build can be accessed in the `/home/vtkm-user/vtk-m/`.
The source code is available in the `vtk-m` directory.
The build files are available in the `vtk-m-build` directory.
The source for the tutorial exercises can be found in the `tutorial` subdirectory of the VTK-m source (i.e., `~/vtk-m/vtk-m-build/tutorial`).
Likewise, the built executables are in the `tutorial` subdirectory of the VTK-m build (i.e., `~/vtk-m/vtk-m-build/tutorial`).

### Running the Docker container

For those who prefer containers, we have included a Docker container with the latest release of VTK-m and the tutorial examples. You will find a Dockerfile and instructions in the `/docker` directory of this repository. A pre-built container image can be downloaded from the Docker hub. If your site does not support Docker but allows Singularity, we have also included instructions to build a Singularity image from the Docker container.

The VTK-m source and build directory can be accessed in directory `/vtkmtutorial`. The source code is in the `vtk-m` subdirectory, whereas the build files are in `vtk-m-build`. To run the tutorial examples navigate to `/vtkmtutorial/vtk-m-build/tutorial`. The docker container has been configured to start in that directory when launched interactively.
 
---

[^gitdownload]: If you have git installed _with git-lfs_ on your system, you can instead simply clone the repository: `git clone --branch release https://gitlab.kitware.com/vtk/vtk-m.git`

## Datasets

[Here](https://www.dropbox.com/sh/2k8fhudvxaaa1s5/AADnMlU2W2A1cE-cwr25kxf1a?dl=0) is a link to a few interesting datasets (VTK format) that you may try visualizing with VTK-m during or after the tutorial. The datasets are the courtesty of the data repository https://klacansky.com/open-scivis-datasets/.
