# Docker container for the VTK-m tutorial

This container provides the release branch of VTK-m including the tutorials.

### Build Docker container

You can build your own container with the Dockerfile provided in this directory. Use the following command line:
```sh
docker build -t name_of_your_container .
```

### Run container from Docker hub interactively
```sh
docker run -it srizzi/vtkm-tutorial:latest
```

### Run with Singularity

If your site allows only Singularity containers, you can convert from Docker to Singularity with the following command line:
```sh
singularity build your_image_name docker://srizzi/vtkm-tutorial:latest
```

### Directory structure

VTK-m is built in the `/vtkmtutorial` directory. To run the tutorial examples navigate to `/vtkmtutorial/vtk-m-build/tutorial`

 
 
